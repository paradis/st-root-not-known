/*
	Project : SPT depth first with election of leader
	Author : Romain Agostinelli, Simon Corboz
	Date : December 2021
*/

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"path/filepath"
	"time"

	"gopkg.in/yaml.v2"
)

var PORT = ":30000"

type yamlConfig struct {
	ID         int         `yaml:"id"`
	Address    string      `yaml:"address"`
	Neighbours []Neighbour `yaml:"neighbours"`
}

// messageKind represents the enumeration of the 4 possibles message kinds.
type messageKind int

const (
	M messageKind = iota // M Adoption request
	P                    // P Accept adoption request
	R                    // R refuse adoption request
	S                    // S stop (terminate)
)

// Message structure used for transfer data between nodes
type Message struct {
	Kind   messageKind `json:"messageKind"`
	Source *NodeImpl   `json:"node"`
	Leader int         `json:"leader"`
}

// Neighbour represent a node that is linked to the actual node. It wraps a NodeImpl (Node interface) with EdgeWeight.
type Neighbour struct {
	NodeImpl   `yaml:",inline"`
	EdgeWeight int `yaml:"edge_weight"`
}

// Node specifies what a Node is capable of.
type Node interface {
	// ID a Node must be capable of giving its id.
	ID() int
	// Addr a Node must be capable of giving its address (IP). This addr can be used without modification to send data.
	// The port must be contained.
	Addr() string
}

// NodeImpl implementation of a node, it has an ID and an Addr.
// attributes are exported only because it is used by yaml decoder. Must not be directly used.
type NodeImpl struct {
	Id      int    `yaml:"id"`
	Address string `yaml:"address"`
}

func (n *NodeImpl) ID() int {
	return n.Id
}

func (n *NodeImpl) Addr() string {
	return n.Address
}

// state represents the state of the actual node Ni. It encapsulates all the data of a node.
type state struct {
	// node the actual representation of this node.
	node *NodeImpl
	// parent the parent node in the Spanning Tree.
	parent Node
	// leader the actual leader to which this node is attached to.
	leader int
	// children all its children nodes.
	children []Node
	// notChildren all the treated neighbors that refused to be its children.
	notChildren []Node
	// neighbors all the neighbors of the node (independent of the SPT protocol).
	neighbors []Neighbour
	// notExploredNeighbors all the neighbors that were not treated.
	notExploredNeighbors []Neighbour
	// terminated stores the status of the STP algo for this node.
	terminated bool
}

// NewMessage creates a Message structure containing the all the information needed inside a Message regarding the actual
// state of this node.
func (s state) NewMessage(k messageKind) Message {
	return Message{
		Kind:   k,
		Leader: s.leader,
		Source: s.node,
	}
}

// initAndParseFileNeighbours initialize the current state for the actual node.
func initAndParseFileNeighbours(filename string, isStartingPoint bool) state {
	fullpath, _ := filepath.Abs("./" + filename)
	yamlFile, err := ioutil.ReadFile(fullpath)

	if err != nil {
		panic(err)
	}

	var data yamlConfig

	err = yaml.Unmarshal(yamlFile, &data)
	if err != nil {
		panic(err)
	}
	res := state{
		node: &NodeImpl{
			Id:      data.ID,
			Address: data.Address,
		},
		parent:               nil,
		leader:               0,
		notExploredNeighbors: make([]Neighbour, 0, len(data.Neighbours)),
		neighbors:            make([]Neighbour, 0, len(data.Neighbours)),
		children:             nil,
		notChildren:          nil,
		terminated:           false,
	}

	res.notExploredNeighbors = append(res.notExploredNeighbors, data.Neighbours...)
	res.neighbors = append(res.neighbors, data.Neighbours...)

	if isStartingPoint {
		res.parent = res.node
		res.leader = res.node.Id
	}

	return res
}

// nodeLog logs regarding a defined format the message.
func nodeLog(n Node, message string) {
	fmt.Printf("[%d] : %s\n", n.ID(), message)
}

// send the body to the specified address. Generally, it's used via the method sendTo.
func send(neighAddress string, body []byte) {
	checkErr := func(err error) {
		if err != nil {
			log.Fatal(err)
		}
	}
	outConn, err := net.Dial("tcp", neighAddress+PORT)
	checkErr(err)
	_, err = outConn.Write(body)
	checkErr(err)
	checkErr(outConn.Close())
}

// sendTo sends a Message to the destination Node.
func sendTo(dest Node, m Message) {
	// build the message
	body, _ := json.Marshal(m) // TODO : Take care of the error
	go send(dest.Addr(), body)
}

// server represents a node algorithm. Make sure that if isStartingPoint = true, it must have already one server without
// isStartingPoint = true launched.
func server(neighboursFilePath string, isStartingPoint bool) {
	// Parse file and init the current state.
	currState := initAndParseFileNeighbours(neighboursFilePath, isStartingPoint)
	nodeLog(currState.node, "Finished file parsing")

	ln, err := net.Listen("tcp", currState.node.Address+PORT)
	if err != nil {
		log.Fatal(err)
		return
	}
	nodeLog(currState.node, "Starting server .... and listening ...")
	nodeLog(currState.node, "Starting algorithm ...")

	if isStartingPoint {
		nodeLog(currState.node, "This NodeImpl is the starting point")
		var neigh Neighbour
		neigh, currState.notExploredNeighbors = currState.notExploredNeighbors[0], currState.notExploredNeighbors[1:]
		go sendTo(&neigh, currState.NewMessage(M))
	}

	for !currState.terminated {
		// Accept all incoming connections
		conn, _ := ln.Accept()
		message, _ := bufio.NewReader(conn).ReadBytes('\n')
		var rcv Message
		err := json.Unmarshal(message, &rcv)
		if err != nil {
			log.Fatalf("Error occured when trying to convert received byte to message: %s  / err: %s", message, err)
		}
		err = conn.Close()
		if err != nil {
			log.Fatalf("Error occured when closing the connection: %s", err)
		}

		// We make different things regarding the type of the message we received.
		switch rcv.Kind {
		case M: // if adoption request
			if currState.leader < rcv.Leader { // Switch to another ST if the leader is bigger
				nodeLog(
					currState.node, fmt.Sprintf(
						"changing leader from %d to %d by (%d)", currState.leader, rcv.Leader,
						rcv.Source.ID(),
					),
				)
				currState.leader = rcv.Leader
				currState.parent = rcv.Source
				// re push everybody into not explored neighbors
				currState.notExploredNeighbors = currState.notExploredNeighbors[:0]
				currState.notExploredNeighbors = append(currState.notExploredNeighbors, currState.neighbors...)
				// remove the received the source from our not explored neighbors
				var i int
				for i = 0; i < len(currState.notExploredNeighbors); i++ {
					if currState.notExploredNeighbors[i].ID() == rcv.Source.ID() {
						break
					}
				}
				// This is safe, we can receive Adoption request only be neighbors, so len(notExploredNeighbors) > 0 &&
				// notExploredNeighbors contains rcv.Node
				currState.notExploredNeighbors[i] = currState.notExploredNeighbors[len(currState.notExploredNeighbors)-1]
				currState.notExploredNeighbors = currState.notExploredNeighbors[:len(currState.notExploredNeighbors)-1]

				// Reset all our children and notChildren
				currState.children = currState.children[:0]
				currState.notChildren = currState.notChildren[:0]

				// if it remains neighbors to explore, we explore them, else accept adoption message
				if len(currState.notExploredNeighbors) > 0 {
					var neigh Neighbour
					neigh, currState.notExploredNeighbors = currState.notExploredNeighbors[0], currState.notExploredNeighbors[1:]
					go sendTo(&neigh, currState.NewMessage(M))
				} else {
					go sendTo(rcv.Source, currState.NewMessage(P))
				}
			} else if currState.leader == rcv.Leader { // In the same ST, refuse adoption request
				go sendTo(rcv.Source, currState.NewMessage(R))
			} else { // The received has not the best leader
				// Do nothing let him wait, ST blocked
			}
		case P, R: // Rejection or Accept of adoption request we sent
			if rcv.Kind == R {
				currState.notChildren = append(currState.notChildren, rcv.Source)
			} else { // if P
				currState.children = append(currState.children, rcv.Source)
			}
			if len(currState.notExploredNeighbors) == 0 { // no remaining neighbor to explore, send to parent
				if currState.parent.ID() != currState.node.ID() {
					sendTo(currState.parent, currState.NewMessage(P))
				} else {
					nodeLog(currState.node, "I am the boss, stopping everybody!")
					currState.terminated = true // I am the final leader
				}
			} else { // it remains neighbors to explore
				var neigh Neighbour
				// pop
				neigh, currState.notExploredNeighbors = currState.notExploredNeighbors[0], currState.notExploredNeighbors[1:]
				go sendTo(&neigh, currState.NewMessage(M))
			}
		case S:
			// received termination
			currState.terminated = true
		}
	}
	// Free everybody by sending broadcast using the spanning tree we just built
	for _, child := range currState.children {
		log.Printf("Node %d -S-> %d \n", currState.node.ID(), child.ID())
		sendTo(child, currState.NewMessage(S))
	}
}

func main() {
	// launch every nodes and wait just a bit before launching starting point nodes
	go server("node-2.yaml", false)
	go server("node-3.yaml", false)
	go server("node-4.yaml", false)
	go server("node-5.yaml", false)
	go server("node-7.yaml", false)
	time.Sleep(2 * time.Second) // Waiting all NodeImpl to be ready
	go server("node-1.yaml", true)
	go server("node-6.yaml", true)
	server("node-8.yaml", true)
	time.Sleep(2 * time.Second) // Waiting all console return from nodes
}
